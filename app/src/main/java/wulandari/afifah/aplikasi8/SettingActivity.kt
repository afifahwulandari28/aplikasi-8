package wulandari.afifah.aplikasi8


import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_setting.*

class SettingActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var pref : SharedPreferences
    val arrayWarnaBackground = arrayOf("Blue", "Yellow", "Green", "Black")
    lateinit var adapterSpin: ArrayAdapter<String>

    override fun onClick(v: View?) {
        pref = getSharedPreferences("setting",Context.MODE_PRIVATE)
        val editor = pref.edit()
        editor.putInt("text",txIsiDetail.text.toString().toInt())
        editor.commit()


    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        adapterSpin = ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayWarnaBackground)
        spBackground.adapter = adapterSpin

        btnSimpanSetting.setOnClickListener(this)


    }


}